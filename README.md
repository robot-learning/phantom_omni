# Phantom Omni

ROS driver for the firewire version of the Phantom Omni haptic input device. This is a fork from [fsuarez6/phantom_omni](https://github.com/fsuarez6/phantom_omni). The primary difference in this fork is that it uses the description files from [ll4ma_robots_description](https://bitbucket.org/robot-learning/ll4ma_robots_description) that have corrected STLs and URDFs for the omni.

## Installation

See [our wiki page](https://robot-learning.cs.utah.edu/phantom_omni) for detailed installation instructions for using the Phantom Omni device on Linux.

## Usage

To test the installation try

    roslaunch phantom_omni omni.launch
	
This should bring up the Omni visualized in rViz and you should be able to move the device around to see the rViz rendering match the real device. Do a `rostopic list` to see all that's being published.

## Troubleshooting
Sometimes you'll see a ROS error about unable to initialize the haptic device. This happens most times after you've restarted the computer. I think some of the symbolic links get wiped out, but also computer updates/kernel changes require the driver to be recompiled. Any time you see the error just do

    rosrun phantom_omni initialize_device.sh -c
	
This recompiles the ROS driver for the Omni and sets symoblic links for the firewire ports in `/dev` and such. Sometimes, when a kernel update has been issued, you might have to go into the source folder for the low-level driver and clean it. To do this, navigate to `phantom_omni/src/dummyraw1394` and issue a `make clean` command. Then try the `rosrun` command above again. If you still get errors, see [our wiki page](https://robot-learning.cs.utah.edu/phantom_omni), you likely have a more involved issue and there is more setup information on that page.

## Related

The [ll4ma_teleop](https://bitbucket.org/robot-learning/ll4ma_teleop) package provides code for commanding both simulated and real robots using the Omni device.